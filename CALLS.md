## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Openstack Nova. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Openstack Nova.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Openstack_nova. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">showDetailsOfSpecificAPIVersion(apiVersion, callback)</td>
    <td style="padding:15px">Show Details of Specific API Version</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServers(accessIpV4, accessIpV6, allTenants, autoDiskConfig, availabilityZone, changesSince, configDrive, createdAt, deleted, description, flavor, host, hostname, image, ip, ip6, kernelId, keyName, launchIndex, launchedAt, limit, lockedBy, marker, name, node, powerState, progress, projectId, ramdiskId, reservationId, rootDeviceName, softDeleted, sortDir, sortKey, status, taskState, terminatedAt, userId, uuid, vmState, notTags, notTagsAny, tags, tagsAny, changesBefore, locked, callback)</td>
    <td style="padding:15px">List Servers</td>
    <td style="padding:15px">{base_path}/{version}/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServer(body, callback)</td>
    <td style="padding:15px">Create Server</td>
    <td style="padding:15px">{base_path}/{version}/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServersDetailed(accessIpV4, accessIpV6, allTenants, autoDiskConfig, availabilityZone, changesSince, configDrive, createdAt, deleted, description, flavor, host, hostname, image, ip, ip6, kernelId, keyName, launchIndex, launchedAt, limit, lockedBy, marker, name, node, powerState, progress, projectId, ramdiskId, reservationId, rootDeviceName, softDeleted, sortDir, sortKey, status, taskState, terminatedAt, userId, uuid, vmState, notTags, notTagsAny, tags, tagsAny, changesBefore, locked, callback)</td>
    <td style="padding:15px">List Servers Detailed</td>
    <td style="padding:15px">{base_path}/{version}/servers/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showServerDetails(serverId, callback)</td>
    <td style="padding:15px">Show Server Details</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateServer(serverId, body, callback)</td>
    <td style="padding:15px">Update Server</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServer(serverId, callback)</td>
    <td style="padding:15px">Delete Server</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAssociateFloatingIpAddFloatingIpActionDEPRECATED(serverId, body, callback)</td>
    <td style="padding:15px">Add (Associate) Floating Ip (addFloatingIp Action) (DEPRECATED)</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConsole(serverId, body, callback)</td>
    <td style="padding:15px">Create Console</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/remote-consoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSecurityGroupsByServer(serverId, callback)</td>
    <td style="padding:15px">List Security Groups By Server</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-security-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showServerDiagnostics(serverId, callback)</td>
    <td style="padding:15px">Show Server Diagnostics</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/diagnostics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIps(serverId, callback)</td>
    <td style="padding:15px">List Ips</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/ips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showIpDetails(serverId, networkLabel, callback)</td>
    <td style="padding:15px">Show Ip Details</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/ips/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllMetadata(serverId, callback)</td>
    <td style="padding:15px">List All Metadata</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateMetadataItems(serverId, body, callback)</td>
    <td style="padding:15px">Create or Update Metadata Items</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceMetadataItems(serverId, body, callback)</td>
    <td style="padding:15px">Replace Metadata Items</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMetadataItemDetails(serverId, key, callback)</td>
    <td style="padding:15px">Show Metadata Item Details</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/metadata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateMetadataItem(serverId, key, callback)</td>
    <td style="padding:15px">Create Or Update Metadata Item</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/metadata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMetadataItem(serverId, key, callback)</td>
    <td style="padding:15px">Delete Metadata Item</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/metadata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listActionsForServer(limit, marker, changesSince, changesBefore, serverId, callback)</td>
    <td style="padding:15px">List Actions For Server</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-instance-actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showServerActionDetails(serverId, requestId, callback)</td>
    <td style="padding:15px">Show Server Action Details</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-instance-actions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPortInterfaces(serverId, callback)</td>
    <td style="padding:15px">List Port Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInterface(serverId, body, callback)</td>
    <td style="padding:15px">Create Interface</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showPortInterfaceDetails(serverId, portId, callback)</td>
    <td style="padding:15px">Show Port Interface Details</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachInterface(serverId, portId, callback)</td>
    <td style="padding:15px">Detach Interface</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showServerPassword(serverId, callback)</td>
    <td style="padding:15px">Show Server Password</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-server-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAdminPassword(serverId, callback)</td>
    <td style="padding:15px">Clear Admin Password</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-server-password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVolumeAttachmentsForAnInstance(limit, offset, serverId, callback)</td>
    <td style="padding:15px">List volume attachments for an instance</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-volume_attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachAVolumeToAnInstance(serverId, body, callback)</td>
    <td style="padding:15px">Attach a volume to an instance</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-volume_attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showADetailOfAVolumeAttachment(serverId, volumeId, callback)</td>
    <td style="padding:15px">Show a detail of a volume attachment</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-volume_attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAVolumeAttachment(serverId, volumeId, body, callback)</td>
    <td style="padding:15px">Update a volume attachment</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-volume_attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachAVolumeFromAnInstance(serverId, volumeId, callback)</td>
    <td style="padding:15px">Detach a volume from an instance</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-volume_attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServerMigrations(serverId, callback)</td>
    <td style="padding:15px">List Migrations</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/migrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showMigrationDetails(serverId, migrationId, callback)</td>
    <td style="padding:15px">Show Migration Details</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/migrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAbortMigration(serverId, migrationId, callback)</td>
    <td style="padding:15px">Delete (Abort) Migration</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/migrations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forceMigrationCompleteActionForceCompleteAction(serverId, migrationId, body, callback)</td>
    <td style="padding:15px">Force Migration Complete Action (force_complete Action)</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/migrations/{pathv2}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTags(serverId, callback)</td>
    <td style="padding:15px">List Tags</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceTags(serverId, body, callback)</td>
    <td style="padding:15px">Replace Tags</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAllTags(serverId, callback)</td>
    <td style="padding:15px">Delete All Tags</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkTagExistence(serverId, tag, callback)</td>
    <td style="padding:15px">Check Tag Existence</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addASingleTag(serverId, tag, callback)</td>
    <td style="padding:15px">Add a Single Tag</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteASingleTag(serverId, tag, callback)</td>
    <td style="padding:15px">Delete a Single Tag</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/tags/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showServerTopology(serverId, callback)</td>
    <td style="padding:15px">Show Server Topology</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVirtualInterfaces(limit, offset, serverId, callback)</td>
    <td style="padding:15px">List Virtual Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/os-virtual-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listsConsoles(serverId, callback)</td>
    <td style="padding:15px">Lists Consoles</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/consoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConsoleForServer(serverId, callback)</td>
    <td style="padding:15px">Create Console</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/consoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showConsoleDetails(serverId, consoleId, callback)</td>
    <td style="padding:15px">Show Console Details</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/consoles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConsole(serverId, consoleId, callback)</td>
    <td style="padding:15px">Delete Console</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/consoles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showConsoleConnectionInformation(consoleToken, callback)</td>
    <td style="padding:15px">Show Console Connection Information</td>
    <td style="padding:15px">{base_path}/{version}/os-console-auth-tokens/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFlavors(sortKey, sortDir, limit, marker, minDisk, minRam, isPublic, callback)</td>
    <td style="padding:15px">List Flavors</td>
    <td style="padding:15px">{base_path}/{version}/flavors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFlavor(body, callback)</td>
    <td style="padding:15px">Create Flavor</td>
    <td style="padding:15px">{base_path}/{version}/flavors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFlavorsWithDetails(sortKey, sortDir, limit, marker, minDisk, minRam, isPublic, callback)</td>
    <td style="padding:15px">List Flavors With Details</td>
    <td style="padding:15px">{base_path}/{version}/flavors/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFlavorDetails(flavorId, callback)</td>
    <td style="padding:15px">Show Flavor Details</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFlavorDescription(flavorId, body, callback)</td>
    <td style="padding:15px">Update Flavor Description</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFlavor(flavorId, callback)</td>
    <td style="padding:15px">Delete Flavor</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFlavorAccessInformationForGivenFlavor(flavorId, callback)</td>
    <td style="padding:15px">List Flavor Access Information For Given Flavor</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}/os-flavor-access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addFlavorAccessToTenantAddTenantAccessAction(flavorId, body, callback)</td>
    <td style="padding:15px">Add Flavor Access To Tenant (addTenantAccess Action)</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtraSpecsForAFlavor(flavorId, callback)</td>
    <td style="padding:15px">List Extra Specs For A Flavor</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}/os-extra_specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createExtraSpecsForAFlavor(flavorId, body, callback)</td>
    <td style="padding:15px">Create Extra Specs For A Flavor</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}/os-extra_specs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAnExtraSpecForAFlavor(flavorId, flavorExtraSpecKey, callback)</td>
    <td style="padding:15px">Show An Extra Spec For A Flavor</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}/os-extra_specs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnExtraSpecForAFlavor(flavorId, flavorExtraSpecKey, body, callback)</td>
    <td style="padding:15px">Update An Extra Spec For A Flavor</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}/os-extra_specs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnExtraSpecForAFlavor(flavorId, flavorExtraSpecKey, callback)</td>
    <td style="padding:15px">Delete An Extra Spec For A Flavor</td>
    <td style="padding:15px">{base_path}/{version}/flavors/{pathv1}/os-extra_specs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listKeypairs(userId, limit, marker, callback)</td>
    <td style="padding:15px">List Keypairs</td>
    <td style="padding:15px">{base_path}/{version}/os-keypairs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrImportKeypair(body, callback)</td>
    <td style="padding:15px">Create Or Import Keypair</td>
    <td style="padding:15px">{base_path}/{version}/os-keypairs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showKeypairDetails(userId, keypairName, callback)</td>
    <td style="padding:15px">Show Keypair Details</td>
    <td style="padding:15px">{base_path}/{version}/os-keypairs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKeypair(userId, keypairName, callback)</td>
    <td style="padding:15px">Delete Keypair</td>
    <td style="padding:15px">{base_path}/{version}/os-keypairs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showRateAndAbsoluteLimits(reserved, tenantId, callback)</td>
    <td style="padding:15px">Show Rate And Absolute Limits</td>
    <td style="padding:15px">{base_path}/{version}/limits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAggregates(callback)</td>
    <td style="padding:15px">List Aggregates</td>
    <td style="padding:15px">{base_path}/{version}/os-aggregates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAggregate(body, callback)</td>
    <td style="padding:15px">Create Aggregate</td>
    <td style="padding:15px">{base_path}/{version}/os-aggregates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAggregateDetails(aggregateId, callback)</td>
    <td style="padding:15px">Show Aggregate Details</td>
    <td style="padding:15px">{base_path}/{version}/os-aggregates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAggregate(aggregateId, body, callback)</td>
    <td style="padding:15px">Update Aggregate</td>
    <td style="padding:15px">{base_path}/{version}/os-aggregates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAggregate(aggregateId, callback)</td>
    <td style="padding:15px">Delete Aggregate</td>
    <td style="padding:15px">{base_path}/{version}/os-aggregates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addHost(aggregateId, body, callback)</td>
    <td style="padding:15px">Add Host</td>
    <td style="padding:15px">{base_path}/{version}/os-aggregates/{pathv1}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestImagePreCachingForAggregate(aggregateId, body, callback)</td>
    <td style="padding:15px">Request Image Pre-caching for Aggregate</td>
    <td style="padding:15px">{base_path}/{version}/os-aggregates/{pathv1}/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAssistedVolumeSnapshots(body, callback)</td>
    <td style="padding:15px">Create Assisted Volume Snapshots</td>
    <td style="padding:15px">{base_path}/{version}/os-assisted-volume-snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssistedVolumeSnapshot(deleteInfo, snapshotId, callback)</td>
    <td style="padding:15px">Delete Assisted Volume Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/os-assisted-volume-snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailabilityZoneInformation(callback)</td>
    <td style="padding:15px">Get Availability Zone Information</td>
    <td style="padding:15px">{base_path}/{version}/os-availability-zone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDetailedAvailabilityZoneInformation(callback)</td>
    <td style="padding:15px">Get Detailed Availability Zone Information</td>
    <td style="padding:15px">{base_path}/{version}/os-availability-zone/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listHypervisors(limit, marker, hypervisorHostnamePattern, withServers, callback)</td>
    <td style="padding:15px">List Hypervisors</td>
    <td style="padding:15px">{base_path}/{version}/os-hypervisors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listHypervisorsDetails(limit, marker, hypervisorHostnamePattern, withServers, callback)</td>
    <td style="padding:15px">List Hypervisors Details</td>
    <td style="padding:15px">{base_path}/{version}/os-hypervisors/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showHypervisorStatisticsDEPRECATED(callback)</td>
    <td style="padding:15px">Show Hypervisor Statistics (DEPRECATED)</td>
    <td style="padding:15px">{base_path}/{version}/os-hypervisors/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showHypervisorDetails(withServers, hypervisorId, callback)</td>
    <td style="padding:15px">Show Hypervisor Details</td>
    <td style="padding:15px">{base_path}/{version}/os-hypervisors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showHypervisorUptimeDEPRECATED(hypervisorId, callback)</td>
    <td style="padding:15px">Show Hypervisor Uptime (DEPRECATED)</td>
    <td style="padding:15px">{base_path}/{version}/os-hypervisors/{pathv1}/uptime?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchHypervisorDEPRECATED(hypervisorHostnamePattern, callback)</td>
    <td style="padding:15px">Search Hypervisor (DEPRECATED)</td>
    <td style="padding:15px">{base_path}/{version}/os-hypervisors/{pathv1}/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listHypervisorServersDEPRECATED(hypervisorHostnamePattern, callback)</td>
    <td style="padding:15px">List Hypervisor Servers (DEPRECATED)</td>
    <td style="padding:15px">{base_path}/{version}/os-hypervisors/{pathv1}/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServerUsageAudits(callback)</td>
    <td style="padding:15px">List Server Usage Audits</td>
    <td style="padding:15px">{base_path}/{version}/os-instance_usage_audit_log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUsageAuditsBeforeSpecifiedTime(beforeTimestamp, callback)</td>
    <td style="padding:15px">List Usage Audits Before Specified Time</td>
    <td style="padding:15px">{base_path}/{version}/os-instance_usage_audit_log/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMigrations(hidden, host, instanceUuid, migrationType, sourceCompute, status, limit, marker, changesSince, changesBefore, userId, projectId, callback)</td>
    <td style="padding:15px">List Migrations</td>
    <td style="padding:15px">{base_path}/{version}/os-migrations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAQuota(userId, tenantId, callback)</td>
    <td style="padding:15px">Show A Quota</td>
    <td style="padding:15px">{base_path}/{version}/os-quota-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateQuotas(userId, tenantId, body, callback)</td>
    <td style="padding:15px">Update Quotas</td>
    <td style="padding:15px">{base_path}/{version}/os-quota-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revertQuotasToDefaults(userId, tenantId, callback)</td>
    <td style="padding:15px">Revert Quotas To Defaults</td>
    <td style="padding:15px">{base_path}/{version}/os-quota-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultQuotasForTenant(tenantId, callback)</td>
    <td style="padding:15px">List Default Quotas For Tenant</td>
    <td style="padding:15px">{base_path}/{version}/os-quota-sets/{pathv1}/defaults?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showTheDetailOfQuota(userId, tenantId, callback)</td>
    <td style="padding:15px">Show The Detail of Quota</td>
    <td style="padding:15px">{base_path}/{version}/os-quota-sets/{pathv1}/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showTheQuotaForQuotaClass(id, callback)</td>
    <td style="padding:15px">Show the quota for Quota Class</td>
    <td style="padding:15px">{base_path}/{version}/os-quota-class-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateQuotasForQuotaClass(id, body, callback)</td>
    <td style="padding:15px">Create or Update Quotas for Quota Class</td>
    <td style="padding:15px">{base_path}/{version}/os-quota-class-sets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listServerGroups(allProjects, limit, offset, callback)</td>
    <td style="padding:15px">List Server Groups</td>
    <td style="padding:15px">{base_path}/{version}/os-server-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServerGroup(body, callback)</td>
    <td style="padding:15px">Create Server Group</td>
    <td style="padding:15px">{base_path}/{version}/os-server-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showServerGroupDetails(serverGroupId, callback)</td>
    <td style="padding:15px">Show Server Group Details</td>
    <td style="padding:15px">{base_path}/{version}/os-server-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServerGroup(serverGroupId, callback)</td>
    <td style="padding:15px">Delete Server Group</td>
    <td style="padding:15px">{base_path}/{version}/os-server-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listComputeServices(binary, host, callback)</td>
    <td style="padding:15px">List Compute Services</td>
    <td style="padding:15px">{base_path}/{version}/os-services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSchedulingForAComputeService(body, callback)</td>
    <td style="padding:15px">Disable Scheduling For A Compute Service</td>
    <td style="padding:15px">{base_path}/{version}/os-services/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSchedulingForAComputeServiceAndLogDisabledReason(body, callback)</td>
    <td style="padding:15px">Disable Scheduling For A Compute Service and Log Disabled Reason</td>
    <td style="padding:15px">{base_path}/{version}/os-services/disable-log-reason?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSchedulingForAComputeService(body, callback)</td>
    <td style="padding:15px">Enable Scheduling For A Compute Service</td>
    <td style="padding:15px">{base_path}/{version}/os-services/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateForcedDown(body, callback)</td>
    <td style="padding:15px">Update Forced Down</td>
    <td style="padding:15px">{base_path}/{version}/os-services/force-down?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateComputeService(serviceId, body, callback)</td>
    <td style="padding:15px">Update Compute Service</td>
    <td style="padding:15px">{base_path}/{version}/os-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteComputeService(serviceId, callback)</td>
    <td style="padding:15px">Delete Compute Service</td>
    <td style="padding:15px">{base_path}/{version}/os-services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTenantUsageStatisticsForAllTenants(detailed, end, start, limit, marker, callback)</td>
    <td style="padding:15px">List Tenant Usage Statistics For All Tenants</td>
    <td style="padding:15px">{base_path}/{version}/os-simple-tenant-usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showUsageStatisticsForTenant(end, start, limit, marker, tenantId, callback)</td>
    <td style="padding:15px">Show Usage Statistics For Tenant</td>
    <td style="padding:15px">{base_path}/{version}/os-simple-tenant-usage/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runEvents(body, callback)</td>
    <td style="padding:15px">Run Events</td>
    <td style="padding:15px">{base_path}/{version}/os-server-external-events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listExtensions(callback)</td>
    <td style="padding:15px">List Extensions</td>
    <td style="padding:15px">{base_path}/{version}/extensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showExtensionDetails(alias, callback)</td>
    <td style="padding:15px">Show Extension Details</td>
    <td style="padding:15px">{base_path}/{version}/extensions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworks(callback)</td>
    <td style="padding:15px">List Networks</td>
    <td style="padding:15px">{base_path}/{version}/os-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetwork(callback)</td>
    <td style="padding:15px">Create Network</td>
    <td style="padding:15px">{base_path}/{version}/os-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetwork(callback)</td>
    <td style="padding:15px">Add Network</td>
    <td style="padding:15px">{base_path}/{version}/os-networks/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showNetworkDetails(networkId, callback)</td>
    <td style="padding:15px">Show Network Details</td>
    <td style="padding:15px">{base_path}/{version}/os-networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetwork(networkId, callback)</td>
    <td style="padding:15px">Delete Network</td>
    <td style="padding:15px">{base_path}/{version}/os-networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateHost(networkId, body, callback)</td>
    <td style="padding:15px">Associate Host</td>
    <td style="padding:15px">{base_path}/{version}/os-networks/{pathv1}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVolumes(limit, offset, callback)</td>
    <td style="padding:15px">List Volumes</td>
    <td style="padding:15px">{base_path}/{version}/os-volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVolume(body, callback)</td>
    <td style="padding:15px">Create Volume</td>
    <td style="padding:15px">{base_path}/{version}/os-volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVolumesWithDetails(limit, offset, callback)</td>
    <td style="padding:15px">List Volumes With Details</td>
    <td style="padding:15px">{base_path}/{version}/os-volumes/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVolumeDetails(volumeId, callback)</td>
    <td style="padding:15px">Show Volume Details</td>
    <td style="padding:15px">{base_path}/{version}/os-volumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVolume(volumeId, callback)</td>
    <td style="padding:15px">Delete Volume</td>
    <td style="padding:15px">{base_path}/{version}/os-volumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSnapshots(limit, offset, callback)</td>
    <td style="padding:15px">List Snapshots</td>
    <td style="padding:15px">{base_path}/{version}/os-snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSnapshot(body, callback)</td>
    <td style="padding:15px">Create Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/os-snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSnapshotsWithDetails(limit, offset, callback)</td>
    <td style="padding:15px">List Snapshots With Details</td>
    <td style="padding:15px">{base_path}/{version}/os-snapshots/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showSnapshotDetails(snapshotId, callback)</td>
    <td style="padding:15px">Show Snapshot Details</td>
    <td style="padding:15px">{base_path}/{version}/os-snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Delete Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/os-snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listImages(changesSince, server, name, status, minDisk, minRam, type, limit, marker, callback)</td>
    <td style="padding:15px">List Images</td>
    <td style="padding:15px">{base_path}/{version}/images?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listImagesWithDetails(changesSince, server, name, status, minDisk, minRam, type, limit, marker, callback)</td>
    <td style="padding:15px">List Images With Details</td>
    <td style="padding:15px">{base_path}/{version}/images/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showImageDetails(imageId, callback)</td>
    <td style="padding:15px">Show Image Details</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteImage(imageId, callback)</td>
    <td style="padding:15px">Delete Image</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listImageMetadata(imageId, callback)</td>
    <td style="padding:15px">List Image Metadata</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createImageMetadata(imageId, body, callback)</td>
    <td style="padding:15px">Create Image Metadata</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateImageMetadata(imageId, body, callback)</td>
    <td style="padding:15px">Update Image Metadata</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showImageMetadataItem(imageId, key, callback)</td>
    <td style="padding:15px">Show Image Metadata Item</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/metadata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateImageMetadataItem(imageId, key, body, callback)</td>
    <td style="padding:15px">Create Or Update Image Metadata Item</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/metadata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteImageMetadataItem(imageId, key, callback)</td>
    <td style="padding:15px">Delete Image Metadata Item</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/metadata/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBareMetalNodes(callback)</td>
    <td style="padding:15px">List Bare Metal Nodes</td>
    <td style="padding:15px">{base_path}/{version}/os-baremetal-nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showBareMetalNodeDetails(nodeId, callback)</td>
    <td style="padding:15px">Show Bare Metal Node Details</td>
    <td style="padding:15px">{base_path}/{version}/os-baremetal-nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProjectNetworks(callback)</td>
    <td style="padding:15px">List Project Networks</td>
    <td style="padding:15px">{base_path}/{version}/os-tenant-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProjectNetwork(callback)</td>
    <td style="padding:15px">Create Project Network</td>
    <td style="padding:15px">{base_path}/{version}/os-tenant-networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showProjectNetworkDetails(networkId, callback)</td>
    <td style="padding:15px">Show Project Network Details</td>
    <td style="padding:15px">{base_path}/{version}/os-tenant-networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProjectNetwork(networkId, callback)</td>
    <td style="padding:15px">Delete Project Network</td>
    <td style="padding:15px">{base_path}/{version}/os-tenant-networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFloatingIpPools(callback)</td>
    <td style="padding:15px">List Floating Ip Pools</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ip-pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFloatingIpAddresses(callback)</td>
    <td style="padding:15px">List Floating Ip Addresses</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAllocateFloatingIpAddress(body, callback)</td>
    <td style="padding:15px">Create (Allocate) Floating Ip Address</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFloatingIpAddressDetails(floatingIpId, callback)</td>
    <td style="padding:15px">Show Floating Ip Address Details</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeallocateFloatingIpAddress(floatingIpId, callback)</td>
    <td style="padding:15px">Delete (Deallocate) Floating Ip Address</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSecurityGroups(limit, offset, allTenants, callback)</td>
    <td style="padding:15px">List Security Groups</td>
    <td style="padding:15px">{base_path}/{version}/os-security-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityGroup(body, callback)</td>
    <td style="padding:15px">Create Security Group</td>
    <td style="padding:15px">{base_path}/{version}/os-security-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showSecurityGroupDetails(securityGroupId, callback)</td>
    <td style="padding:15px">Show Security Group Details</td>
    <td style="padding:15px">{base_path}/{version}/os-security-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityGroup(securityGroupId, body, callback)</td>
    <td style="padding:15px">Update Security Group</td>
    <td style="padding:15px">{base_path}/{version}/os-security-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityGroup(securityGroupId, callback)</td>
    <td style="padding:15px">Delete Security Group</td>
    <td style="padding:15px">{base_path}/{version}/os-security-groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityGroupRule(body, callback)</td>
    <td style="padding:15px">Create Security Group Rule</td>
    <td style="padding:15px">{base_path}/{version}/os-security-group-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityGroupRule(securityGroupRuleId, callback)</td>
    <td style="padding:15px">Delete Security Group Rule</td>
    <td style="padding:15px">{base_path}/{version}/os-security-group-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listHosts(callback)</td>
    <td style="padding:15px">List Hosts</td>
    <td style="padding:15px">{base_path}/{version}/os-hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showHostDetails(hostName, callback)</td>
    <td style="padding:15px">Show Host Details</td>
    <td style="padding:15px">{base_path}/{version}/os-hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHostStatus(hostName, body, callback)</td>
    <td style="padding:15px">Update Host status</td>
    <td style="padding:15px">{base_path}/{version}/os-hosts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootHost(hostName, callback)</td>
    <td style="padding:15px">Reboot Host</td>
    <td style="padding:15px">{base_path}/{version}/os-hosts/{pathv1}/reboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">shutDownHost(hostName, callback)</td>
    <td style="padding:15px">Shut Down Host</td>
    <td style="padding:15px">{base_path}/{version}/os-hosts/{pathv1}/shutdown?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startHost(hostName, callback)</td>
    <td style="padding:15px">Start Host</td>
    <td style="padding:15px">{base_path}/{version}/os-hosts/{pathv1}/startup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRootCertificate(callback)</td>
    <td style="padding:15px">Create Root Certificate</td>
    <td style="padding:15px">{base_path}/{version}/os-certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showRootCertificateDetails(callback)</td>
    <td style="padding:15px">Show Root Certificate Details</td>
    <td style="padding:15px">{base_path}/{version}/os-certificates/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCloudpipes(callback)</td>
    <td style="padding:15px">List Cloudpipes</td>
    <td style="padding:15px">{base_path}/{version}/os-cloudpipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCloudpipe(body, callback)</td>
    <td style="padding:15px">Create Cloudpipe</td>
    <td style="padding:15px">{base_path}/{version}/os-cloudpipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCloudpipe(body, callback)</td>
    <td style="padding:15px">Update Cloudpipe</td>
    <td style="padding:15px">{base_path}/{version}/os-cloudpipe/configure-project?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingInstances(allTenants, include, exclude, callback)</td>
    <td style="padding:15px">Ping Instances</td>
    <td style="padding:15px">{base_path}/{version}/os-fping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pingAnInstance(instanceId, callback)</td>
    <td style="padding:15px">Ping An Instance</td>
    <td style="padding:15px">{base_path}/{version}/os-fping/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showFixedIpDetails(fixedIp, callback)</td>
    <td style="padding:15px">Show Fixed Ip Details</td>
    <td style="padding:15px">{base_path}/{version}/os-fixed-ips/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reserveOrReleaseAFixedIp(fixedIp, body, callback)</td>
    <td style="padding:15px">Reserve Or Release A Fixed Ip</td>
    <td style="padding:15px">{base_path}/{version}/os-fixed-ips/{pathv1}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFloatingIps(callback)</td>
    <td style="padding:15px">List Floating Ips</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ips-bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFloatingIps(body, callback)</td>
    <td style="padding:15px">Create Floating Ips</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ips-bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkDeleteFloatingIps(body, callback)</td>
    <td style="padding:15px">Bulk-Delete Floating Ips</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ips-bulk/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFloatingIpsByHost(hostName, callback)</td>
    <td style="padding:15px">List Floating Ips By Host</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ips-bulk/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDNSDomains(callback)</td>
    <td style="padding:15px">List DNS Domains</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ip-dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateDNSDomain(domain, callback)</td>
    <td style="padding:15px">Create Or Update DNS Domain</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ip-dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSDomain(domain, callback)</td>
    <td style="padding:15px">Delete DNS Domain</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ip-dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDNSEntries(domain, ip, callback)</td>
    <td style="padding:15px">List DNS Entries</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ip-dns/{pathv1}/entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUniqueDNSEntry(domain, name, callback)</td>
    <td style="padding:15px">Find Unique DNS Entry</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ip-dns/{pathv1}/entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateDNSEntry(domain, name, body, callback)</td>
    <td style="padding:15px">Create Or Update DNS Entry</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ip-dns/{pathv1}/entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDNSEntry(domain, name, callback)</td>
    <td style="padding:15px">Delete DNS Entry</td>
    <td style="padding:15px">{base_path}/{version}/os-floating-ip-dns/{pathv1}/entries/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCells(limit, offset, tenantId, callback)</td>
    <td style="padding:15px">List Cells</td>
    <td style="padding:15px">{base_path}/{version}/os-cells?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCell(callback)</td>
    <td style="padding:15px">Create Cell</td>
    <td style="padding:15px">{base_path}/{version}/os-cells?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">capacities(callback)</td>
    <td style="padding:15px">Capacities</td>
    <td style="padding:15px">{base_path}/{version}/os-cells/capacities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCellsWithDetails(limit, offset, callback)</td>
    <td style="padding:15px">List Cells With Details</td>
    <td style="padding:15px">{base_path}/{version}/os-cells/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">infoForThisCell(callback)</td>
    <td style="padding:15px">Info For This Cell</td>
    <td style="padding:15px">{base_path}/{version}/os-cells/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showCellData(cellId, callback)</td>
    <td style="padding:15px">Show Cell Data</td>
    <td style="padding:15px">{base_path}/{version}/os-cells/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateACell(cellId, callback)</td>
    <td style="padding:15px">Update a Cell</td>
    <td style="padding:15px">{base_path}/{version}/os-cells/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteACell(cellId, callback)</td>
    <td style="padding:15px">Delete a Cell</td>
    <td style="padding:15px">{base_path}/{version}/os-cells/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showCellCapacities(cellId, callback)</td>
    <td style="padding:15px">Show Cell Capacities</td>
    <td style="padding:15px">{base_path}/{version}/os-cells/{pathv1}/capacities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultSecurityGroupRules(callback)</td>
    <td style="padding:15px">List Default Security Group Rules</td>
    <td style="padding:15px">{base_path}/{version}/os-security-group-default-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDefaultSecurityGroupRule(body, callback)</td>
    <td style="padding:15px">Create Default Security Group Rule</td>
    <td style="padding:15px">{base_path}/{version}/os-security-group-default-rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showDefaultSecurityGroupRuleDetails(securityGroupDefaultRuleId, callback)</td>
    <td style="padding:15px">Show Default Security Group Rule Details</td>
    <td style="padding:15px">{base_path}/{version}/os-security-group-default-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDefaultSecurityGroupRule(securityGroupDefaultRuleId, callback)</td>
    <td style="padding:15px">Delete Default Security Group Rule</td>
    <td style="padding:15px">{base_path}/{version}/os-security-group-default-rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAgentBuilds(hypervisor, callback)</td>
    <td style="padding:15px">List Agent Builds</td>
    <td style="padding:15px">{base_path}/{version}/os-agents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAgentBuild(body, callback)</td>
    <td style="padding:15px">Create Agent Build</td>
    <td style="padding:15px">{base_path}/{version}/os-agents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAgentBuild(agentBuildId, body, callback)</td>
    <td style="padding:15px">Update Agent Build</td>
    <td style="padding:15px">{base_path}/{version}/os-agents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAgentBuild(agentBuildId, callback)</td>
    <td style="padding:15px">Delete Agent Build</td>
    <td style="padding:15px">{base_path}/{version}/os-agents/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runServerAction(serverId, body, callback)</td>
    <td style="padding:15px">Perform an action on a server.</td>
    <td style="padding:15px">{base_path}/{version}/servers/{pathv1}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
