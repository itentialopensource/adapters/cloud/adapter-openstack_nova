
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_19:56PM

See merge request itentialopensource/adapters/adapter-openstack_nova!15

---

## 0.3.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-openstack_nova!13

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:05PM

See merge request itentialopensource/adapters/adapter-openstack_nova!12

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:18PM

See merge request itentialopensource/adapters/adapter-openstack_nova!11

---

## 0.3.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!10

---

## 0.2.4 [03-28-2024]

* Changes made at 2024.03.28_13:12PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!8

---

## 0.2.3 [03-21-2024]

* Changes made at 2024.03.21_13:49PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!7

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_15:29PM

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!6

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_11:41AM

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!5

---

## 0.2.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!4

---

## 0.1.4 [12-11-2023]

* moved sso section to sample properties

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!3

---

## 0.1.3 [01-09-2023]

* moved sso section to sample properties

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!3

---

## 0.1.2 [12-14-2022]

* added new task for server actions endpoint

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!2

---

## 0.1.1 [08-31-2022]

* Bug fixes and performance improvements

See commit 451fb3d

---
