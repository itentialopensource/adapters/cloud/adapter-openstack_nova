# Openstack Nova

Vendor: Openstack
Homepage: https://www.openstack.org/

Product: Nova
Product Page: https://docs.openstack.org/nova/latest/

## Introduction
We classify OpenStack Nova into the Cloud domain as Nova is a cloud computing service that provides scalable and on-demand compute resources for deploying and managing virtual machines.

## Why Integrate
The OpenStack Nova adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nova. With this adapter you have the ability to perform operations on items such as:

- Images
- Snapshots
- Volumes
- Servers

## Additional Product Documentation
The [API documents for OpenStack Nova](https://docs.openstack.org/api-ref/compute/)