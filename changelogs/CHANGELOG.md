
## 0.1.3 [01-09-2023]

* moved sso section to sample properties

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!3

---

## 0.1.2 [12-14-2022]

* added new task for server actions endpoint

See merge request itentialopensource/adapters/cloud/adapter-openstack_nova!2

---

## 0.1.1 [08-31-2022]

* Bug fixes and performance improvements

See commit 451fb3d

---
